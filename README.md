# Image annotater


[![pipeline status](https://gitlab.com/rishi93/image-annotater/badges/main/pipeline.svg)](https://gitlab.com/rishi93/image-annotater/-/commits/main) 


![Coverage status](./app/coverage.svg)


* A user can create a "PROJECT" for each image classification task.
* Under a project, there are multiple images, each belonging to a certain category
* So we need to store these two pieces of data (image and it's annotation) and link
them together

# Live Demo
http://fastapi-server-env.eba-fggrfkvm.eu-central-1.elasticbeanstalk.com/docs

# Image annotation format
We'll store the annotation for each image in JSON format
```json
    project_name: "...", # The project that an image belongs to
    image_name: "..." # The filename for an image
    image_size: [x, y] # The image dimensions (width, height)
    image_dpi: [x, y] # The image dpi (if available)
    image_url: "..." # The storage location for this image
    category: "..." # The category to be used for image classification
}
```

# CI/CD pipeline:
The pipeline has three stages and is triggered on each commit in `main` branch

- build: build a docker image and push it to the container registry
- test: pull image from registry run tests for the api endpoints
- deploy: pull the docker image and run it on AWS Beanstalk

# Architecture
* The FastAPI server is running inside a Docker container.
* The server communicates with a managed MongoDB instance (MongoAtlas) to store image annotations
* The server stores the images in a AWS S3 bucket