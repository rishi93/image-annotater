import os

import motor.motor_asyncio
import pytest
import pytest_asyncio
from httpx import AsyncClient

from config.database import get_db
from server.app import app


def override_get_db():
    mongo_atlas_user = os.getenv("MONGO_ATLAS_USER")
    mongo_atlas_pass = os.getenv("MONGO_ATLAS_PASS")
    mongo_connection_string = f"mongodb+srv://{mongo_atlas_user}:{mongo_atlas_pass}@fastapi-mongo.dpssfvt.mongodb.net/?retryWrites=true&w=majority"
    client = motor.motor_asyncio.AsyncIOMotorClient(mongo_connection_string)
    db = client.test_annotations
    return db


app.dependency_overrides[get_db] = override_get_db

client = AsyncClient(app=app, base_url="http://0.0.0.0")


@pytest.mark.asyncio
async def test_get_root():
    """Test if the root route works fine"""
    response = await client.get("/")

    assert response.status_code == 200
    assert response.json() == {"message": "Hello world!"}


@pytest_asyncio.fixture(scope="session")
async def setup_db():
    db = override_get_db()
    # setup test database
    # Make sure we start with a blank test database
    # just in case someone played with the test database
    await db.projects.delete_many({})
    await db.projects.delete_many({})
    await db.projects.insert_one({"project_name": "emotions"})
    yield
    # teardown test database
    await db.projects.delete_many({})
    await db.imageAnnotations.delete_many({})


class TestProjects:
    @pytest.mark.asyncio
    async def test_get_projects(self, setup_db):
        """Test if the GET /projects route works fine"""
        response = await client.get("/projects/")

        assert response.status_code == 200
        assert response.json() == ["emotions"]

    @pytest.mark.asyncio
    async def test_post_projects_already_exists(self, setup_db):
        """Test if the POST /projects route works with a project name that
        already exists
        """
        project_name = "emotions"
        response = await client.post(f"/projects/?project_name={project_name}")

        assert response.status_code == 400
        assert response.json() == {"message": f"Project {project_name} already exists"}

    @pytest.mark.asyncio
    async def test_post_projects_new_name(self, setup_db):
        """Test if the POST /projects route works with a new project name"""
        project_name = "faces"
        response = await client.post(f"/projects/?project_name={project_name}")

        assert response.status_code == 200
        assert response.json() == {"message": f"Created new project {project_name}"}


class TestImageAnnotations:
    @pytest.mark.asyncio
    async def test_post_image(self, setup_db):
        filename = f"{os.getcwd()}/resources/smiling_person.png"
        project_name = "emotions"
        category = "happy"

        with open(filename, "rb") as file:
            response = await client.post(
                f"/images/?project_name={project_name}&category={category}",
                files={"file": file},
            )

        assert "project_name" in response.json()
        assert "image_name" in response.json()
        assert "image_size" in response.json()
        assert "image_dpi" in response.json()
        assert "image_url" in response.json()
        assert "category" in response.json()

    @pytest.mark.asyncio
    async def test_get_annotations(self, setup_db):
        project_name = "emotions"
        response = await client.get(f"/images/?project_name={project_name}")

        assert response.status_code == 200
        assert type(response.json()) == list
        assert "project_name" in response.json()[0]
        assert "image_name" in response.json()[0]
        assert "image_size" in response.json()[0]
        assert "image_dpi" in response.json()[0]
        assert "image_url" in response.json()[0]
        assert "category" in response.json()[0]
