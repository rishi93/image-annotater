"""
    This module specifies all the routes of our webserver
"""

from fastapi import FastAPI

from server.routes import ImageAnnotationsRouter, ProjectsRouter

app = FastAPI()

# All the endpoints related to creating or getting projects
app.include_router(ProjectsRouter, tags=["Projects"], prefix="/projects")
# All the endpoints related to uploading and downloading image data
app.include_router(ImageAnnotationsRouter, tags=["Image Annotations"], prefix="/images")


@app.get("/", tags=["Healthcheck"])
async def get_root():
    """
    Basic healthcheck endpoint to check if server is up
    """
    return {"message": "Hello world!"}
