"""
    This module contains all the functions related to uploading files
    to our block storage
"""
import os

import boto3
from dotenv import load_dotenv

load_dotenv()

from fastapi import UploadFile


class S3Storage:
    """
    This class contains methods are interact with the S3 storage
    """

    class ImageUploadError(Exception):
        """
        Exception to be raised if Image upload fails
        """

    bucket_name = os.getenv("S3_BUCKET_NAME")

    s3_client = boto3.client(
        "s3",
        aws_access_key_id=os.getenv("ACCESS_KEY"),
        aws_secret_access_key=os.getenv("SECRET_ACCESS_KEY"),
    )

    @classmethod
    def store_image(cls, project_name: str, category: str, file: UploadFile) -> str:
        try:
            # contents = file.file.read()
            # file.file.seek(0)
            key_name = f"{project_name}/{category}/{file.filename}"
            # Upload the file to your s3 service
            cls.s3_client.upload_fileobj(file.file, "rishi-images-bucket", key_name)
        except Exception as e:
            print(e)
            raise cls.ImageUploadError
        finally:
            file.file.close()
            image_url = "{}/{}/{}".format(
                cls.s3_client.meta.endpoint_url, cls.bucket_name, key_name
            )
            return image_url
