import os

import motor.motor_asyncio
from dotenv import load_dotenv
from motor.motor_asyncio import AsyncIOMotorDatabase

"""
This line below is only for local development, make sure you have the .env file
locally

In production, the env variables are set automatically
"""
load_dotenv()


def get_db() -> AsyncIOMotorDatabase:
    """Returns a database object that can be interacted with.

    In test environment, this will be switched to return the test database

    Returns:
        AsyncIOMotorDatabase: Database object
    """
    mongo_atlas_user = os.getenv("MONGO_ATLAS_USER")
    mongo_atlas_pass = os.getenv("MONGO_ATLAS_PASS")
    mongo_connection_string = f"mongodb+srv://{mongo_atlas_user}:{mongo_atlas_pass}@fastapi-mongo.dpssfvt.mongodb.net/?retryWrites=true&w=majority"
    client = motor.motor_asyncio.AsyncIOMotorClient(mongo_connection_string)
    db = client.annotations
    return db
