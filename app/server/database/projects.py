from typing import Optional

from bson import ObjectId

from server.database.errors import ProjectAlreadyExists
from server.models import Project


class ProjectsDatabase:
    """
    This class contains all methods that interact with the projects collection
    """

    @staticmethod
    async def get_project_by_name(db, project_name: str) -> Optional[Project]:
        """Return a project with the given project_name

        Args:
            project_name (str): Project name

        Returns:
            Optional[Project]: Project object (if found else None)
        """
        project = await db.projects.find_one({"project_name": project_name})

        return project

    @staticmethod
    async def get_project_by_id(db, project_id: ObjectId) -> Optional[Project]:
        """Return a project with the given project_id

        Args:
            project_id (ObjectId): project_id

        Returns:
            Optional[Project]: Project data (if exists else None)
        """
        new_project_data = await db.projects.find_one({"_id": ObjectId(project_id)})

        return new_project_data

    @staticmethod
    async def insert_project(db, project_name: str) -> Project:
        """Insert project with name project_name into projects collection

        Args:
            project_name (str): project name (should be unique name)

        Returns:
            Project: Project data
        """
        new_project = await db.projects.insert_one({"project_name": project_name})

        return new_project

    @classmethod
    async def add_project_name(cls, db, project_name: str) -> Project:
        """Add project name to project collections

        Args:
            project_name (str): new project name

        Returns:
            Project: project data if project with name project_name doesn't
            already exist

        Raises:
            ProjectAlreadyExists: if project if name project_name already exists
        """
        # Check if project name already exists
        project = await cls.get_project_by_name(db, project_name)

        # If project name already exists, don't create it
        if project:
            raise ProjectAlreadyExists

        # If project name doesn't already exist, create it
        new_project = await cls.insert_project(db, project_name)
        new_project_data = await cls.get_project_by_id(db, new_project.inserted_id)

        return new_project_data

    @staticmethod
    async def get_project_names(db) -> list[str]:
        """Get list of all project names from database

        Returns:
            list[str]: List of all projects names in database
        """
        all_projects = []
        async for project in db.projects.find({}):
            all_projects.append(project.get("project_name"))
        return all_projects
