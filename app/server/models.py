"""
    This module contains all the Pydantic models for the Data that we store
    in the MongoDB database. Also the Response Model
"""

from bson import ObjectId
from pydantic import BaseModel


class Project(BaseModel):
    """
    Datamodel for the objects stored in the projects collection
    """

    project_name: str


class AnnotationData(BaseModel):
    """
    Annotation Data that is supplied to the Mongo database

    For some images, PIL (Python Imaging Library is unable to read the dpi information)
    """

    project_name: str
    image_name: str
    image_size: tuple[int, int]
    image_dpi: tuple[float, float] | None
    image_url: str
    category: str


class ImageAnnotation(AnnotationData):
    """
    Datamodel for the objects stored in the image annotations collection.
    """

    _id: ObjectId


class ResponseModel(BaseModel):
    """
    Datamodel for the response returned from the API
    """

    message: str
