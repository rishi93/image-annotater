from fastapi import APIRouter, Depends, Response, status

from config.database import get_db
from server.database import ProjectAlreadyExists, ProjectsDatabase
from server.models import ResponseModel

router = APIRouter()


@router.get("/", response_model=list[str] | ResponseModel)
async def get_all_projects(db=Depends(get_db)):
    """
    Endpoint to get list of all projects created so far
    """
    response = await ProjectsDatabase.get_project_names(db)
    if not response:
        return {"message": "No projects yet created"}
    return response


@router.post("/", response_model=ResponseModel)
async def create_project(project_name: str, response: Response, db=Depends(get_db)):
    """
    Endpoint to create a new project with name project_name
    """
    try:
        project = await ProjectsDatabase.add_project_name(db, project_name)
    except ProjectAlreadyExists:
        response.status_code = status.HTTP_400_BAD_REQUEST
        return {"message": f"Project {project_name} already exists"}

    return {"message": f"Created new project {project.get('project_name')}"}
