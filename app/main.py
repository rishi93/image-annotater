"""
    This module is the entry point of the application. It runs
    the app application from the server.app module on a uvicorn server.
"""

import uvicorn

if __name__ == "__main__":
    uvicorn.run("server.app:app", host="0.0.0.0", port=80, reload=True)
