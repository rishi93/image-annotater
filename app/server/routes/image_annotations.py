from fastapi import APIRouter, Depends, Response, UploadFile, status
from fastapi.encoders import jsonable_encoder
from PIL import Image

from config.database import get_db
from server.database import ImageAnnotationsDatabase, ProjectsDatabase
from server.models import ImageAnnotation, ResponseModel
from server.storage import S3Storage

# from server.tasks import handle_zip_file

router = APIRouter()


@router.post("/", response_model=ImageAnnotation | ResponseModel)
async def upload_training_data(
    project_name: str,
    file: UploadFile,
    category: str,
    response: Response,
    db=Depends(get_db),
):
    """
    Upload training data
    """
    # Our endpoint only accepts png or jpeg files
    valid_file_types = ["image/png", "image/jpeg"]

    # Check if project exists
    project = await ProjectsDatabase.get_project_by_name(db, project_name)
    if not project:
        return {"message": f"Project {project_name} does not exist"}

    # Check if file is of valid type
    if file.content_type not in valid_file_types:
        response.status_code = status.HTTP_400_BAD_REQUEST
        return {"message": "Invalid file type! Only jpeg/png images accepted."}

    # Load the image in memory
    image = Image.open(file.file)
    image_size = image.size
    image_dpi = image.info.get("dpi")

    # Store the image in s3 bucket
    try:
        image_url = S3Storage.store_image(project_name, category, file)
    except S3Storage.ImageUploadError:
        return {"message": "Could not store image"}

    # Add image annotation
    annotation_data = {
        "project_name": project_name,
        "image_name": file.filename,
        "image_size": image_size,
        "image_dpi": image_dpi,
        "image_url": image_url,
        "category": category,
    }

    # Add the image annotation to database
    image_annotation = jsonable_encoder(annotation_data)
    new_image_annotation = await ImageAnnotationsDatabase.add_image_annotation(
        db, image_annotation
    )

    # Return the successful response
    return new_image_annotation


@router.get("/", response_model=list[ImageAnnotation] | ResponseModel)
async def get_training_data(project_name: str, db=Depends(get_db)):
    """
    Get training data
    """
    annotations = await ImageAnnotationsDatabase.retrieve_annotations(db, project_name)
    if annotations:
        return annotations
    return {"message": f"no image annotations found in {project_name}"}
