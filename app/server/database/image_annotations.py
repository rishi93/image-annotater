from bson.objectid import ObjectId

from server.models import AnnotationData, ImageAnnotation


class ImageAnnotationsDatabase:
    """
    This class contains all the methods that interact with the imageAnnotations
    collection
    """

    @staticmethod
    async def get_image_annotation_by_id(
        db, image_annotation_id: ObjectId
    ) -> ImageAnnotation:
        """Retrieve an Image Annotation from database given it's id

        Args:
            db: config_db
            image_annotation_id (ObjectId): id of ImageAnnotation

        Returns:
            ImageAnnotation
        """
        new_image_annotation = await db.imageAnnotations.find_one(
            {"_id": ObjectId(image_annotation_id)}
        )

        return new_image_annotation

    @staticmethod
    async def insert_image_annotation(
        db, image_annotation: AnnotationData
    ) -> ImageAnnotation:
        """_summary_

        Args:
            db: config_db
            image_annotation: AnnotationData

        Returns:
            ImageAnnotation: ImageAnnotation successfully added to database
        """
        new_image_annotation = await db.imageAnnotations.insert_one(image_annotation)

        return new_image_annotation

    @classmethod
    async def add_image_annotation(cls, db, annotation_data):
        """_summary_

        Args:
            db (_type_): _description_
            annotation_data (_type_): _description_

        Returns:
            _type_: _description_
        """
        new_image_annotation = await cls.insert_image_annotation(db, annotation_data)

        image_annotation = await cls.get_image_annotation_by_id(
            db, new_image_annotation.inserted_id
        )

        return image_annotation

        # return self.image_annotation_helper(image_annotation)

    @classmethod
    async def retrieve_annotations(cls, db, project_name: str):
        """Retrieve all image annotations for a project

        Args:
            project_name (str): project name

        Returns:
            list[ImageAnnotation]: list of image annotations
        """
        image_annotations = []

        async for image_annotation in db.imageAnnotations.find(
            {"project_name": project_name}
        ):
            image_annotation = cls.image_annotation_helper(image_annotation)
            image_annotations.append(image_annotation)
        return image_annotations

    @classmethod
    def image_annotation_helper(cls, image_annotation) -> dict:
        """To stringify the ObjectId

        Args:
            image_annotation (_type_): _description_

        Returns:
            dict: _description_
        """
        return {
            "id": str(image_annotation["_id"]),
            "project_name": image_annotation["project_name"],
            "image_name": image_annotation["image_name"],
            "image_size": image_annotation["image_size"],
            "image_dpi": image_annotation["image_dpi"],
            "image_url": image_annotation["image_url"],
            "category": image_annotation["category"],
        }
